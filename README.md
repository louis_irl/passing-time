# Passing Time
Passing Time is a simple utility to play audio files at a regular interval.  It was created to be used as a replacement for a traditional class bell in my high school.

## Dependencies
* Python 3
* pydub

## Usage
1. Place audio files in 'songs' folder
2. Set the playback length and times to play at in settings.py.
3. Run main.py