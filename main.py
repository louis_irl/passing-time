#!/usr/bin/env python3
from pydub import AudioSegment
from pydub.playback import play
from settings import *
import time, os, random

# Convert the song length set in settings.py into milliseconds for pydub
songLength = songLength * 1000

while True:

    # Store the names of all files in ./songs in a list, and choose a random one to be chosenSong
    allFiles = os.listdir('./songs')
    randomSong = random.randint(0, len(allFiles) - 1)
    chosenSong = allFiles[randomSong]

    # cut chosen song to chosen length of time
    cutSong = AudioSegment.from_mp3('./songs/' + chosenSong)[:songLength]

    # Continuously check if current time is one of chosen times, and play if so
    while True:
        if time.strftime("%H:%M") in times:
            play(cutSong)
            break
